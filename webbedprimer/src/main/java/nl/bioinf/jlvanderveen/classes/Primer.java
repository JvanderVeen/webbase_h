package nl.bioinf.jlvanderveen.classes;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class Primer {
    private String name;
    private String sequence;
    private Float gc;
    private Float meltpoint;
    private Integer homopolymer;
    // Intermolecular identity compares the sequences with each other, thus only 1 integer,
    // intramolecular identity however compares the sequence individually, thus 1 or 2 intergers
    private Integer interidentity;
    private Integer intraidentity;

    // Since this is a bean class, we need the method Primer() even though it doesn't really
    // do anything
    public Primer() {
    }

    public Primer(String name, String sequence) {
        this.name = name;
        this.sequence = sequence;
        // We can analyse as much as possible with the already known data.
        // (basically everything except for the intermolecular identity)
        analyseSequence();
    }

    public String getName() {
        return name;
    }

    public String getSequence() {
        return sequence;
    }

    public void setGc(Float gc) {
        this.gc = gc;
    }

    public void setIntraidentity(Integer intraidentity) {
        this.intraidentity = intraidentity;
    }

    public void setMeltpoint(Float meltpoint) {
        this.meltpoint = meltpoint;
    }

    public void setHomopolymer(Integer homopolymer) {
        this.homopolymer = homopolymer;
    }


    private void analyseSequence() {
        // This method is a mini pipeline, it calls the methods that can calculate one
        // specific trait of the sequence.
        Float gc = calculateGC(this.sequence);
        setGc(gc);
        Float melt = calculateMeltpoint(this.sequence, gc);
        setMeltpoint(melt);
        Integer homopolymer = findHomopolymer(this.sequence);
        setHomopolymer(homopolymer);
        Integer intra = calculateMolecularIdentity(this.sequence);
        setIntraidentity(intra);
    }

    private Float calculateMeltpoint(String sequence, Float gc) {
        // For this method, the formula from http://insilico.ehu.es/tm.php?formula=basic (date:6-5-2020)
        // was used to calculate the meltingpoint.
        Float meltpoint;
        Integer length = sequence.length() - 6;
        // We can avoid going through the sequence again by using the previous calculated
        // GC% at the cost of accuracy (because of rounding errors)
        Float totalGC = length * (gc / 100);
        if (length < 14) {
            meltpoint = 2 * (length - totalGC) + 4 * (totalGC);
        } else {
            meltpoint = (float) (64.9 + 41 * (totalGC - 16.4) / length);
        }
        return meltpoint;
    }

    private Float calculateGC(String sequence) {
        // While count itself is an integer, dividing an integer with another integer (count/seq.length)
        // will be rounded down, which will return either 0.0 or 100.0% with nothing in between
        // thus count is casted to a float which doesn't suffer the same issue.
        float count = 0;
        // Since the sequence string contains the 3' and 5', we need to start at the 4th
        // character in the string (first nucleotide), we also need to stop 3 characters earlier
        // (last nucleotide).
        for (int i = 3; i < (sequence.length() - 3); i++) {
            // Use a simple or statement to avoid 2 if statements.
            if (sequence.charAt(i) == 'G' || sequence.charAt(i) == 'C') {
                count++;
            }
        }
        Float gc = count / (sequence.length() - 6) * 100;
        return gc;
    }


    private Integer findHomopolymer(String sequence) {
        // The hashmap is used to count the maximal homopolymer per base so we can select the maximal afterwards
        HashMap<Character, Integer> countMax = new HashMap();
        countMax.put('A', 0);
        countMax.put('T', 0);
        countMax.put('G', 0);
        countMax.put('C', 0);

        // Define the last character an illegal character so we can define it in the first loop
        char previous = ' ';
        // Since the sequence should have at least 1 nucleotide (probably more), we can put 
        // the count at 1 rather than 0.
        Integer count = 1;
        // Since the sequence string contains the 3' and 5', we need to start at the 4th
        // character in the string (first nucleotide), we also need to stop 3 characters earlier
        // (last nucleotide).
        for (int i = 3; i < (sequence.length() - 3); i++) {
            char compare = sequence.charAt(i);
            if (compare == previous) {
                count++;
                // only update the countMax if the current sequence is longer
                if (Integer.max(count, countMax.get(compare)) == count) {
                    countMax.put(compare, count);
                }
            } else {
                count = 1;
            }
            previous = compare;
        }
        Integer homopolymer = Collections.max(countMax.values());
        // Because of the way it was counted it would be possible to return a homopolymer of 0
        // even though it would need to be at least 1 (since the sequence needs to contain
        // nucleotides)
        homopolymer = Integer.max(1, homopolymer);
        return homopolymer;
    }

    private Integer calculateMolecularIdentity(String sequence) {
        // Create a hashmap with the complementary nucleotides.
        HashMap<Character, Character> complementary = new HashMap<>();
        complementary.put('A', 'T');
        complementary.put('T', 'A');
        complementary.put('G', 'C');
        complementary.put('C', 'G');

        String reverse;
        // One of the sequences needs to have it's 3' end as the first end.
        // to achieve this, we need to look at the first character in the sequence and act
        // accordingly.
        if (sequence.charAt(0) == '5') {
            reverse = new StringBuilder(sequence).reverse().toString();
        }
        // Sequence can only start with 3 or 5,
        // thus there is no need to put an else if charAt(0)=='3'
        else {
            reverse = sequence;
            sequence = new StringBuilder(sequence).reverse().toString();
        }

        // loop checks whether the first character in the 3'- sequence matches the character
        // since this needs to be a guaranteed match. As explained in the Homopolymer and GC%
        // for loops, we use different starting positions.
        for (int f = 3; f < (sequence.length() - 3); f++) {
            if (complementary.get(sequence.charAt(f)) == reverse.charAt(3)) {
                int count = 4;
                // loop that checks whether the rest of the sequences also match.
                for (int c = f + 1; c < (sequence.length() - 3); c++) {
                    // We use a separate count for both sequences, since the reverse sequence
                    // will (probably) start more in the front than the other sequence.
                    if (complementary.get(sequence.charAt(c)) == reverse.charAt(count)) {
                        // In order to check whether the matching is completed,
                        // we look at the resulting substrings. Since simply checking the 
                        // last nucleotide could be a fluke.
                        if (sequence.substring(f, c).equals(sequence.substring(f, sequence.length() - 4))) {
                            // The final amount of nucleotides would equal the
                            // length total sequence - position of the first matching sequence in the
                            // forward sequence. the -3 is to remove the final 3'- format.
                            return (sequence.length() - 3 - f);

                        }
                    }
                    // If even 1 nucleotide doesn't match, it isn't matchable, thus we can
                    // break the CURRENT loop. It is possible that another starting point will
                    // result in a good match.
                    else {
                        break;
                    }
                    count++;
                }
            }
        }
        // return 0 if no matches can be found
        return 0;
    }

    public Integer calculateInterMolecularIdentity(String seq2) {
        // A lot of this method is inline with the calculateMolecularIdentity() method.
        // For any confusing it is wise to check it's documentation.
        
        // Since we want to compare this primer to another one, we make this sequence
        // the first sequence.
        String seq1 = this.sequence;
        HashMap<Character, Character> complementary = new HashMap<>();
        complementary.put('A', 'T');
        complementary.put('T', 'A');
        complementary.put('G', 'C');
        complementary.put('C', 'G');
        // Make sure that sequence 1 and 2 are in the right order
        if (seq2.charAt(0) == '5') {
            seq2 = new StringBuilder(seq2).reverse().toString();
        }

        if (seq1.charAt(0) == '3') {
            seq1 = new StringBuilder(seq1).reverse().toString();
        }

        // loop checks whether the first character in the 3'- sequence matches the character
        // since this needs to be a guaranteed match.
        for (int f = 3; f < (sequence.length() - 3); f++) {
            if (complementary.get(seq1.charAt(f)) == seq2.charAt(3)) {
                int count = 4;
                // loop for comparing rest of the sequence
                for (int c = f + 1; c < (seq1.length() - 3); c++) {

                    if (complementary.get(seq1.charAt(c)) == seq2.charAt(count)) {
                        if (seq1.substring(f, c).equals(seq1.substring(f, seq1.length() - 4))) {
                            //If the sequences have found a match, the length of this match
                            // is added to the Interidentity variable.
                            setInteridentity(seq1.length() - 3 - f);
                            return (seq1.length() - 3 - f);

                        }
                    } else {
                        break;
                    }
                    count++;
                }
            }
        }
        // return 0 if no matches can be found
        setInteridentity(0);
        return 0;
    }


    public Float getGc() {
        return gc;
    }

    public Float getMeltpoint() {
        return meltpoint;
    }

    public Integer getHomopolymer() {
        return homopolymer;
    }

    public Integer getInteridentity() {
        return interidentity;
    }

    public Integer getIntraidentity() {
        return intraidentity;
    }

    public void setInteridentity(Integer interidentity) {
        this.interidentity = interidentity;
    }
}