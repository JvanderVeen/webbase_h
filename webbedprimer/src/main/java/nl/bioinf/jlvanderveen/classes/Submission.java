package nl.bioinf.jlvanderveen.classes;

import java.util.ArrayList;
import java.util.List;

public class Submission {
    public static List<List<Primer>> Submissions;
    // We create a DB everything a Submission class is created.
    public Submission(){
        this.Submissions = createFresh();
    }

    public List<List<Primer>> getSubmissions() {
        return Submissions;
    }

    public void add(List<Primer>toAdd){
        // We only need to delete an entry if the DB already possesses 5 submissions.
        // And we only want to add the new list if it isn't empty.
        if (this.Submissions.size() == 5 && toAdd != null && !toAdd.isEmpty()){
            this.Submissions.remove(0);
            this.Submissions.add(toAdd);
        }
        else if (toAdd != null && !toAdd.isEmpty()){
        this.Submissions.add(toAdd);
        }


    }
    private List<List<Primer>> createFresh(){
        // Sloppy way to create fake instances (very unrealistic examples) for our 'database'
        // Note that all primers are put in separate lists, this is because the objects would
        // otherwise overwrite each other, meaning that the last example would be copied 5 times.
        ArrayList<List<Primer>> Submissions = new ArrayList<>();
        Primer primer1;
        Primer primer2;
        List<Primer> list = new ArrayList<>();
        primer1 = new Primer("first Primer F", "3'-ATGCATGCATGC-'5");
        primer2 = new Primer("first Primer R", "3'-TGCAACGTTATA-'5");
        primer1.calculateInterMolecularIdentity(primer2.getSequence());
        primer2.calculateInterMolecularIdentity(primer1.getSequence());
        list.add(primer1);
        list.add(primer2);
        Submissions.add(list);

        Primer primer3 = new Primer("second Primer F", "5'-AGTCACGT-'3");
        Primer primer4 = new Primer("second Primer R", "3'-AGCTGGACT-'5");
        primer3.calculateInterMolecularIdentity(primer4.getSequence());
        primer4.calculateInterMolecularIdentity(primer3.getSequence());
        List<Primer> list2 = new ArrayList<>();
        list2.add(primer3);
        list2.add(primer4);
        Submissions.add(list2);

        Primer primer5 = new Primer("lonesome Primer", "5'-GGTCAACGT-'3");
        List<Primer> list3 = new ArrayList<>();
        list3.add(primer5);
        Submissions.add(list3);

        Primer primer6 = new Primer("sequence1", "5'-CTGAAACGTTCA-'3");
        Primer primer7 = new Primer("sequence2", "5'-AGTCACGTGGCT-'3");
        primer6.calculateInterMolecularIdentity(primer7.getSequence());
        primer7.calculateInterMolecularIdentity(primer6.getSequence());
        List<Primer> list4 = new ArrayList<>();
        list4.add(primer6);
        list4.add(primer7);
        Submissions.add(list4);
        Primer primer8 = new Primer("rushed primer", "3'-CCCCCGTAGA-'5");
        Primer primer9 = new Primer("rushed pimer2", "5'-TCTACGGGGG-'3");
        primer8.calculateInterMolecularIdentity(primer9.getSequence());
        primer9.calculateInterMolecularIdentity(primer8.getSequence());
        List<Primer> list5 = new ArrayList<>();
        list5.add(primer8);
        list5.add(primer9);
        Submissions.add(list5);
        return Submissions;
    }
}
