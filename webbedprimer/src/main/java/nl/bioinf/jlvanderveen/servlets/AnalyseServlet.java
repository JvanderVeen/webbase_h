package nl.bioinf.jlvanderveen.servlets;

import nl.bioinf.jlvanderveen.classes.Primer;
import nl.bioinf.jlvanderveen.classes.Submission;
import nl.bioinf.jlvanderveen.config.WebConfig;
import org.thymeleaf.context.WebContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@WebServlet(name = "AnalyseServlet", urlPatterns = "/analyse", loadOnStartup = 1)
public class AnalyseServlet extends HttpServlet {
    // Initialise the database only when first creating the AnalyseServlet since it would
    // otherwise keep resetting the DB.
    public Submission submissions = new Submission();
    @Override
    public void init() throws ServletException {
        System.out.println("Initializing Analyse template engine");
        final ServletContext servletContext = this.getServletContext();
        WebConfig.createTemplateEngine(servletContext);
    }
    private static final long serialVersionUID = 1L;
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
        process(request, response);
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
        process(request, response);
    }
    public void process(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        WebConfig.configureResponse(response);
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        ServletContext servletcontext = super.getServletContext();
        // Create variables to give to the webcontext
        String warning = new String();
        String head1 = request.getParameter("head1");
        String seq1 = request.getParameter("seq1");
        // The pattern all sequences need to match in order to be considered a good sequence.
        Pattern pattern = Pattern.compile("[35]'-[AGTC]+-'[35]");
        List<Primer> list = new ArrayList<>();

        // Flow control that tests whether the first primer is viable if it fails, give appropriate warnings
        if (head1 != null && seq1 != null){
            Matcher matcher = pattern.matcher(seq1.toUpperCase());
            // If the sequence is legal we make the first primer and check whether
            // a second primer was submitted.
            if (matcher.matches()){
                Primer prime1 = new Primer(head1, seq1.toUpperCase());
                // Since updating a webcontext variable isn't that hard we add primer1 to the
                // context straight away, even though we don't know whether we need to compare
                // it to a second primer.
                ctx.setVariable("prime1", prime1);
                String head2 = request.getParameter("head2");
                String seq2 = request.getParameter("seq2");

                if (head2 != "" && seq2 != ""){
                    Matcher matcher2 = pattern.matcher(seq2.toUpperCase());
                    if (matcher2.matches()){
                        // create the second primer and update the first primer, since
                        // the intermolecularidentity can be calculated.
                        // Note that the intermolecular identity is calculated both ways,
                        // since both primers should be updated with this information.
                        Primer prime2 = new Primer(head2, seq2.toUpperCase());
                        prime2.calculateInterMolecularIdentity(prime1.getSequence());
                        prime1.calculateInterMolecularIdentity(prime2.getSequence());
                        // We add these primers to a list so we can add them to the database
                        // later on. We also make these primers available in the webcontext.
                        list.add(prime1);
                        list.add(prime2);
                        ctx.setVariable("prime1", prime1);
                        ctx.setVariable("prime2", prime2);
                    }
                    // The warning given to the user when sequence2 is illegal
                    else {
                        warning = "Sequence2 contained illegal characters,  " +
                                "this website only accepts characters that represent bases directly (ATGC). " +
                                "If the sequence was copy pasted from a FASTA file, keep in mind that " +
                                "FASTA files can contain 'special' characters (like N) that represent " +
                                "multiple nucleotides. The sequence should specify the primers  " +
                                "following the format 3'-ATGC-'5 or 5'-ATGC-'3.";

                    }
                }
                // While technically not an error, since only 1 primer is also viable,
                // we still report that a second primer was missing just in case that the
                // user forgot to put it in.
                // We also add primer1 to the list now, since if we did it earlier, before
                // we knew if the second primer existed, we would need to update the list if
                // there was another primer to compare it to (since this is the first chance
                // that the second primer was meant to be empty, we add it here instead).
                else{
                    warning = "The second primer had missing data. While the analysis can be done with " +
                            "1 sequence, and now will be done using 1 sequence, this warning was given " +
                            "in case the second primer was forgotten.";
                    list.add(prime1);
                }
            }
            // The warning given if sequence1 contained illegal characters.
            else{
                 warning = "Sequence1 contained illegal characters or was not properly formatted,  " +
                        "this website only accepts characters that represent bases directly (ATGC). " +
                        "If the sequence was copy pasted from a FASTA file, keep in mind that " +
                        "FASTA files can contain 'special' characters (like N) that represent " +
                        "multiple nucleotides. The sequence should specify the primers  " +
                        "following the format 3'-ATGC-'5 or 5'-ATGC-'3.";

            }
        }
        // We add the list to the submissions here, even though we don't use it here (we
        // only need it in the historyServlet).
        submissions.add(list);
        ctx.setVariable("warning", warning);
        WebConfig.createTemplateEngine(servletcontext).process("submit", ctx, response.getWriter());
    }
}
