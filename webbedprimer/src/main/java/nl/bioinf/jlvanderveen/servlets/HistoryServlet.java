package nl.bioinf.jlvanderveen.servlets;

import nl.bioinf.jlvanderveen.config.WebConfig;
import org.thymeleaf.context.WebContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "HistoryServlet", urlPatterns = "/history", loadOnStartup = 1)
public class HistoryServlet extends HttpServlet {
    // Since we want the analyseServlet to update the database, it is possible to
    // make a new analysesServlet object which will be used to set the database variable.
    AnalyseServlet analyseServlet = new AnalyseServlet();
    @Override
    public void init() throws ServletException {
        System.out.println("Initializing History template engine");
        final ServletContext servletContext = this.getServletContext();
        WebConfig.createTemplateEngine(servletContext);
    }
    private static final long serialVersionUID = 1L;
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
        process(request, response);
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
        process(request, response);
    }
    public void process(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        WebConfig.configureResponse(response);
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        ServletContext servletcontext = super.getServletContext();
        // The database is gotten from the analyseServlet, since this allows it to update it
        // effectively whilst also being able to load the initial database.
        ctx.setVariable("submissions", analyseServlet.submissions);
        WebConfig.createTemplateEngine(servletcontext).process("database", ctx, response.getWriter());
    }


}
